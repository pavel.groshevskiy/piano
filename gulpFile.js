const { src, dest, parallel, series, watch } = require('gulp');
const browserSync       = require('browser-sync').create();
const concat            = require('gulp-concat');
const uglify            = require('gulp-uglify-es').default;
const sass              = require('gulp-sass');
const autoprefixer      = require('gulp-autoprefixer');
const cleancss          = require('gulp-clean-css');
const imagemin          = require('gulp-imagemin');
const newer             = require('gulp-newer');
const del               = require('del');

function scripts() {
    return src([
        'node_modules/jquery/dist/jquery.min.js', 
        'app/js/app.js',
    ])
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(dest('app/js/'))
    .pipe(browserSync.stream())
}

function browserSyncConnect() {
    browserSync.init({
        server: {baseDir: 'app/'},
        notify: false,
        online: true
    })
};

function styles () {
    return src('app/sass/main.sass')
    .pipe(sass())
    .pipe(concat('app.min.css'))
    .pipe(autoprefixer({overrideBrowserslist: ['last 10 versions'], grid: true}))
    .pipe(cleancss(( { level: { 1: {specialComments: 0 }}/* format: 'beautify'*/ } )))
    .pipe(dest('app/css/'))
    .pipe(browserSync.stream())
}

function images () {
    return src('app/images/src/**/*')
    .pipe(newer('app/images/minimg/'))
    .pipe(imagemin())
    .pipe(dest('app/images/minimg/'))
}

function cleanImg () {
    return del('app/images/minimg/**/*');
}

function cleanDest () {
    return del('app/dest/**/*');
}

function buildCopy () {
    return src([
        'app/css/**/*.min.css',
        'app/js/**/*.min.js',
        'app/images/minimg/**/*',
        'app/**/*.html'
    ], { base: 'app'})
    .pipe(dest('dest'));
}

function startWatch() {
    watch('app/**/*.sass', styles);
    watch(['app/**/*.js','!app/**/*.min.js'], scripts);
    watch('app/**/*.html').on('change', browserSync.reload);
    watch('app/images/src/**/*', images)
}

exports.browserSyncConnect = browserSyncConnect;
exports.scripts            = scripts;
exports.styles             = styles;
exports.images             = images;
exports.cleanImg           = cleanImg;
exports.build              = series(styles, scripts, images, buildCopy);

exports.default            = parallel(cleanDest, styles, scripts, browserSyncConnect, startWatch)